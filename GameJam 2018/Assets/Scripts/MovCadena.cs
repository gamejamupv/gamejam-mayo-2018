﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovCadena : MonoBehaviour
{

    public Rigidbody2D myrb2d;
    public float pOWER = 10;
    public Player hijo;
    public MovCadena actual;

    // Use this for initialization
    void Start()
    {
        //myrb2d = GetComponent<Rigidbody2D>();
        //hijo = GetComponentInChildren<Player>();
        //actual = GetComponent<MovCadena>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            moviendo();
        }

    }

    void moviendo()
    {
        myrb2d.AddForce(new Vector2(Input.GetAxisRaw("Horizontal") * pOWER, myrb2d.velocity.y));
    }

    public void Push(Vector2 vector)
    {
        myrb2d = GetComponent<Rigidbody2D>();
        hijo = GetComponentInChildren<Player>();
        actual = GetComponent<MovCadena>();
        myrb2d.AddForce(new Vector2(vector.x * 10, vector.y));
        myrb2d.mass += 1;
        
    }

    public void Actualiza() {
        myrb2d = GetComponent<Rigidbody2D>();
        hijo = GetComponentInChildren<Player>();
        actual = GetComponent<MovCadena>();
    }
   
}
