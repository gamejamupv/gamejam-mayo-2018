﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTriggerPJ : MonoBehaviour
{
    private Player padre;

    private void Start()
    {
        padre = GetComponentInParent<Player>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Chain")
        {
            
            padre.hijoDeCollider(collision);
        }
    }
}