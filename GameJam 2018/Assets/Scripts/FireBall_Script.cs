﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall_Script : MonoBehaviour {
    private Vector3 target = Vector3.zero;
    private Vector3 initialPos = Vector3.zero;

   

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (target != Vector3.zero)
        {
            //transform.position = Vector3.MoveTowards(transform.position, target,0.5f);
            
            //transform.Translate(new Vector2(initialPos.x - target.x , initialPos.y - target.y) * Time.deltaTime);

            transform.position += new Vector3(initialPos.x - target.x, initialPos.y - target.y, initialPos.z - target.z) * -Time.deltaTime;

        }
    }
        public void Target(Vector3 target) {
        this.target = target;
        initialPos = transform.position;
        Quaternion rotation = Quaternion.LookRotation
             (target - transform.position, transform.TransformDirection(Vector3.up));
        transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
    }
}
