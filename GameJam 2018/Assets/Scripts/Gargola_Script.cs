﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gargola_Script : MonoBehaviour
{
    private Animator myAnim;
    private Transform posPersonaje;
    private Vector3 posIni;
    bool arriba = false;
    bool firing = false;
    public GameObject FireBall;
    // Use this for initialization
    void Start()
    {
        myAnim = GetComponentInParent<Animator>();
        posIni = transform.position + Vector3.up * 5;
    }

    // Update is called once per frame
    void Update()
    {
       

        if (arriba && transform.position.y < posIni.y)
        {
            transform.position += Vector3.up * Time.deltaTime * 5;
        }

        

    }
    private void LateUpdate()
    {

        myAnim.SetBool("IsFlying", arriba);
        myAnim.SetBool("IsFiring", firing);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            arriba = true;
            posPersonaje = collision.transform;
            InvokeRepeating("Fire", 1f, 1f);


        }
    }

    

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            arriba = false;
            CancelInvoke();

        }
    }



    void Fire()
    {
        firing = true;
        GameObject clone;
        clone = Instantiate(FireBall, transform.position, transform.rotation) as GameObject;
        clone.GetComponent<FireBall_Script>().Target(posPersonaje.position);
        Destroy(clone, 5.0f);
    }

}
