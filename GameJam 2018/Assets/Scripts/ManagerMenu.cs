﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerMenu : MonoBehaviour {
    private AudioSource clip;
    public void Awake()
    {
        clip = GetComponent<AudioSource>();
        clip.Play();
    }


    public void Play()
    {
        SceneManager.LoadScene("Nivel 1");
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void Creditos()
    {
        SceneManager.LoadScene("Creditos");
    }
    public void Controles()
    {
        SceneManager.LoadScene("Controles1");
    }


}
