﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento_recto : MonoBehaviour {

    public Transform target;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += Vector3.right * Time.deltaTime;
        if (target.position.x - transform.position.x > 40) { transform.position = new Vector3(target.position.x - 10, target.position.y, target.position.z); }
    }
}
