﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private AudioSource clip;
    [Header("Character")]
    private Transform child;
    private Rigidbody2D myRigidbody;
    public float jumpPower = 5f; //Para indicar la potencia de salto
    public bool doubleJumpAllowed = false; //Si NO powerup, false
    public bool secondJump = false; //Para detectar si se ha producido el segundo salto
    private bool isDying = false; //Para detectar si el pj esta muerto
    public float velocity = 2f; //Para indicar la velocidad del pj
    public float currentVelocity; //Velocidad actual
    public float maxVelocity;
    private int direction;
    private bool isOnAir;
    public bool isOnChain;
    public bool isGrounded;
    public bool isJumped;
    public bool isRunning;
    private GameObject trigger;
    private MovCadena movCadenaScript;
    private float up_Down;
    private bool _CanMoveVertical = true;
    public bool die = false;

    Vector3 puntoInicio;


    [Header("Animation")]
    private Animator myAnim;


    // Use this for initialization
    void Start()
    {
        clip = GetComponent<AudioSource>();
        myRigidbody = GetComponent<Rigidbody2D>();
        myAnim = GetComponentInChildren<Animator>();
        child = transform.GetChild(0);
        isGrounded = true;
        trigger = transform.GetChild(1).gameObject;
        puntoInicio = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab)) { ReSpawn(); }
        if (isDying) { return; } //Aplicar efecto a la muerte
        direction = 0;
        currentVelocity = Input.GetAxisRaw("Horizontal");
        up_Down = Input.GetAxisRaw("Vertical");
        if (isJumped) { currentVelocity *= 0.75f; }


        if ((isGrounded || isOnChain) && Input.GetButtonDown("Jump")) { Jump(); }
        

        if (currentVelocity < 0)
        {
            direction = -1;
            Flip();
            Move();


        }
        else if (currentVelocity > 0)
        {
            direction = 1;
            Flip();
            Move();
        }
        if (!isOnChain)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, 0.2f);
        }

        Grab();

        if (myRigidbody.velocity.y < 0)
        {
            myRigidbody.velocity += Vector2.up * Physics2D.gravity.y * 0.5f * Time.deltaTime;
        }
        else if (myRigidbody.velocity.y > 0 && Input.GetButtonDown("Jump"))
        {

            myRigidbody.velocity += Vector2.up * Physics2D.gravity.y * 1.5f * Time.deltaTime;
        }

        UpDown();
    }

    private void UpDown()
    {
        float _updown = Input.GetAxisRaw("Vertical");
        if (!_CanMoveVertical)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(0,0,0),0.1f);
            transform.localRotation = Quaternion.Lerp(transform.localRotation,Quaternion.identity,0.01f);
        }
        if (isOnChain && _updown != 0 && _CanMoveVertical)
        {
            _CanMoveVertical = false;
            if (movCadenaScript == null)
                return;
           // Debug.Log(movCadenaScript.transform.GetSiblingIndex());
            if (_updown == 1)
            {
                //Debug.Log(movCadenaScript.transform.GetSiblingIndex());
                if (movCadenaScript.transform.GetSiblingIndex() != 0)
                {
                    transform.SetParent(movCadenaScript.transform.parent.GetChild(movCadenaScript.transform.GetSiblingIndex()-1));
                    
                }
            }
            else
            {
               // Debug.Log(movCadenaScript.transform.GetSiblingIndex() - 1);
                if (movCadenaScript.transform.GetSiblingIndex() != movCadenaScript.transform.parent.childCount-1)
                {
                    transform.SetParent(movCadenaScript.transform.parent.GetChild(movCadenaScript.transform.GetSiblingIndex()+1));
                }
            }
            //transform.rotation = Quaternion.identity;
            movCadenaScript.enabled = false;
            Invoke("ResetPosition",0.1f);
        }
    }
    void ResetPosition()
    {
        _CanMoveVertical = true;
        transform.localPosition = Vector3.zero;// GetComponentInParent<Transform>().position;
        //transform.position = Vector3.Lerp(transform.position, Vector3.zero,0.1f);//new Vector3(transform.position.x, 0, transform.position.z);
        movCadenaScript = transform.parent.GetComponent<MovCadena>();
        movCadenaScript.Actualiza();
        movCadenaScript.enabled = true;
    }

    
    public void Flip()
    {
        child.localScale = new Vector3(direction * Mathf.Abs(child.localScale.x), child.localScale.y, 1);

    }
    public void Move()
    {
        myRigidbody.velocity = new Vector2(currentVelocity * velocity, myRigidbody.velocity.y);
    }
    public void Jump()
    {
        isJumped = true;
        isGrounded = false;
        isOnChain = false;
        if (movCadenaScript)
        {
            myRigidbody.bodyType = RigidbodyType2D.Dynamic;
            transform.parent = null;
            movCadenaScript.enabled = false;

        }

        if(!Input.GetButtonDown("Grab"))myRigidbody.AddForce(new Vector2(myRigidbody.velocity.x, jumpPower), ForceMode2D.Impulse);


    }

    public void hijoDeCollider(Collider2D collision)
    {
        if (isOnChain == false)
        {
            movCadenaScript = collision.GetComponent<MovCadena>();
            Vector2 aux = myRigidbody.velocity;
            isOnChain = true;
            isJumped = false;
            myRigidbody.transform.SetParent(collision.transform);
            myRigidbody.bodyType = RigidbodyType2D.Static;
            collision.GetComponent<MovCadena>().enabled = true;
            movCadenaScript.Push(new Vector2(aux.x * 5, aux.y));
        }
    }

    void LateUpdate()
    {
        isRunning = currentVelocity != 0;
        myAnim.SetBool("IsJump", isJumped);
        myAnim.SetBool("IsRunning", currentVelocity != 0);
        myAnim.SetBool("OnRope", isOnChain);
        myAnim.SetBool("IsClimbing", up_Down != 0 && isOnChain);
        myAnim.SetBool("Is Dead", die);
    }

    void Grab()
    {
        if (Input.GetButtonDown("Grab"))
        {
            if (isOnChain)
                Jump();
            else
            {
                trigger.SetActive(true);
                Invoke("UnGrab", 0.1f);
            }
        }
    }

    void UnGrab()
    {
        trigger.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ground")
            isGrounded = true;
        if (col.gameObject.tag == "Lava")
        {
            clip.Play();
            Debug.Log("Muerto");
            Invoke("ReSpawn", 0.5f);
            die = true;
        }

        if (col.gameObject.tag == "enemigo")
        {
            clip.Play();

            Debug.Log("Enemigo");
            Invoke("ReSpawn", 0.5f);
            die = true;
        }

        if (col.gameObject.tag == "Lava_Azul")
        {
            clip.Play();

            Debug.Log("Enemigo");
            Invoke("ReSpawn", 0.5f);
            die = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "enemigo") {
            clip.Play();

            Debug.Log("Por Favor");
            Invoke("ReSpawn", 0.5f);
            die = true;
        }

       
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
            isGrounded = false;
    }

    public void ReSpawn() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

   
