﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovEnemyHorizontal : MonoBehaviour
{

    
    public float dir;
    public float enemyVelocity;
    public Transform right;
    public Transform left;
    
 

    void Start()
    {
        
        dir = 1f;
        enemyVelocity = 5f;
       
    }


    void Update()
    {
        transform.position += Vector3.right * dir * Time.deltaTime * enemyVelocity;
        if (transform.position.x < left.position.x)
        {
            dir = 1;
        }
        if (transform.position.x > right.position.x)
        {
            dir = -1;
        }
    }
}
