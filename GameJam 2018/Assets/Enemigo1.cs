﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo1 : MonoBehaviour {

    public float dir;
    public float enemyVelocity;
    public Transform top;
    public Transform bot;
	

	void Start () {
        dir = 1;
        enemyVelocity = 2.5f;
    }

	void Update ( ) {

        transform.position += Vector3.up* dir * Time.deltaTime * enemyVelocity;
        if (top.position.y < transform.position.y) {
            dir = -1;
        }
        if (bot.position.y > transform.position.y) {
            dir = 1;
        }
	}

    
}
